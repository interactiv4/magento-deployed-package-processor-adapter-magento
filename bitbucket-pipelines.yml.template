definitions:
  script-anchor:
    - &entrypoint /docker-entrypoint.sh
    - &apt_init apt-get update -y && apt-get install -y git zip unzip curl jq procps
    - &composer_selfupdate composer selfupdate
    - &composer_update composer update
    - &unit_tests vendor/bin/phpunit
    - &grumphp_tests vendor/bin/grumphp run
    - &custom_testing chmod -R +x ./bitbucket-pipelines/* && ([ ! -f './bitbucket-pipelines/custom-testing.sh' ] || ./bitbucket-pipelines/custom-testing.sh)

  images:
    php-70: &php-70-image
      name: PHP 7.0
      image: uqppa47/magento2:php-7.0-cli
    php-71: &php-71-image
      name: PHP 7.1
      image: uqppa47/magento2:php-7.1-cli
    php-72: &php-72-image
      name: PHP 7.2
      image: uqppa47/magento2:php-7.2-cli
    php-73: &php-73-image
      name: PHP 7.3
      image: uqppa47/magento2:php-7.3-cli

  tests-definition: &tests
    script:
      - *entrypoint
      - *apt_init
      - *composer_selfupdate
      - *composer_update
      - *unit_tests
      - *grumphp_tests
      - *custom_testing
    caches:
      - composer

  test-suites:
    php-70: &test-suite-php-70
      <<: *php-70-image
      <<: *tests
    php-71: &test-suite-php-71
      <<: *php-71-image
      <<: *tests
    php-72: &test-suite-php-72
      <<: *php-72-image
      <<: *tests
    php-73: &test-suite-php-73
      <<: *php-73-image
      <<: *tests
    all: &test-suite-all
      - parallel:
        - step: *test-suite-php-70
        - step: *test-suite-php-71
        - step: *test-suite-php-72
        - step: *test-suite-php-73

pipelines:
  custom:
    All: *test-suite-all
    PHP 7.0:
      - step: *test-suite-php-70
    PHP 7.1:
      - step: *test-suite-php-71
    PHP 7.2:
      - step: *test-suite-php-72
    PHP 7.3:
      - step: *test-suite-php-73

  branches:
    feature/**: *test-suite-all
    hotfix/**: *test-suite-all
    bugfix/**: *test-suite-all
    release/**: *test-suite-all

  pull-requests:
    feature/**: *test-suite-all
    hotfix/**: *test-suite-all
    bugfix/**: *test-suite-all
    release/**: *test-suite-all
